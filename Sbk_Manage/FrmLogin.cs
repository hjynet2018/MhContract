﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Sbk_Manage
{
    public partial class FrmLogin : Form
    {
        public static string[] args;
        public FrmLogin()
        {
            InitializeComponent();
            //创建窗体句柄时发生
            this.HandleCreated += MainForm_HandleCreated;
            UsernameTextBox.KeyPress += new KeyPressEventHandler(UsernameTextBox_KeyPress);
            PasswordTextBox.KeyPress += new KeyPressEventHandler(PasswordTextBox_KeyPress);
        }
        private void UsernameTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(System.Windows.Forms.Keys.Enter))
            {
                PasswordTextBox.Focus();
            }
        }
        private void PasswordTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(System.Windows.Forms.Keys.Enter))
            {
                OK_Click(null,null);
            }
        }
        public void MainForm_HandleCreated(Object sender, EventArgs e)
        {
            foreach (string arg in args)
            {
                char[] separator = { '|' };
                string[] strs = arg.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                if (strs.Length == 2)
                {
                    this.UsernameTextBox.Text = strs[0];
                    this.PasswordTextBox.Text = strs[1];
                }
                break;
            }
            this.BringToFront();
        }
        //登录麦禾信息官网
        private void label2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.maihe.info/");
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (!UsernameTextBox.Text.Trim().Equals("") && !PasswordTextBox.Text.Trim().Equals(""))
            {
                if (Program.SqliteDB.UserCheck(UsernameTextBox.Text.Trim(), PasswordTextBox.Text.Trim()))
                {
                    //当前用户名
                    Program.User_Name = UsernameTextBox.Text.Trim();
                    Program.User_Pwd = PasswordTextBox.Text.Trim();
                    //登录成功
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("用户名或密码输入有误!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    UsernameTextBox.Focus();
                }
            }
            else
            {
                MessageBox.Show("用户名和密码都不能为空!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            label2.Text = "山东麦禾信息技术有限公司版权所有©2010-" + DateTime.Now.ToString("yyyy");
            this.UsernameTextBox.Text = "";
            this.PasswordTextBox.Text = "";
            this.UsernameTextBox.Select();
            this.UsernameTextBox.Focus();
        }
    }
}
